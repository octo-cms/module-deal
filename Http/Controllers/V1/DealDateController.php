<?php

declare(strict_types=1);

namespace OctoCmsModule\Deal\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use OctoCmsModule\Deal\Entities\DealDate;
use OctoCmsModule\Deal\Http\Requests\UpdateDealDateRequest;
use OctoCmsModule\Deal\Transformers\DealDateResource;

/**
 * @link     https://octopus.srl
 *
 * @category Octo
 * @package  OctoCmsModule\Deal\Http\Controllers\V1
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 */
class DealDateController extends Controller
{
    /**
     * Name update
     *
     * @param mixed $id SaleDate Id
     *
     * @return JsonResponse|object
     *
     * @throws AuthorizationException
     */
    public function update(UpdateDealDateRequest $request, $id)
    {
        /** @var DealDate $dealDate */
        $dealDate = DealDate::findOrFail($id);

        $this->authorize('update', DealDate::class);

        $fields = $request->validated();
        // phpcs:disable
        $dealDate->date_start = Arr::get($fields, 'start', '');
        $dealDate->date_end = Arr::get($fields, 'end', '');
        $dealDate->update();
        // phpcs:enable
        return (new DealDateResource($dealDate))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }
}

<?php

declare(strict_types=1);

namespace OctoCmsModule\Deal\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Deal\Entities\Deal;
use OctoCmsModule\Deal\Entities\DealDate;
use OctoCmsModule\Deal\Http\Requests\GetSaleDatesRequest;
use OctoCmsModule\Deal\Interfaces\DealServiceInterface;
use OctoCmsModule\Deal\Transformers\DealDateResource;
use OctoCmsModule\Deal\Transformers\DealStatusTransitionResource;

use function response;

/**
 * Description ...
 *
 * @link     https://octopus.srl
 *
 * @category Octo
 * @package  OctoCmsModule\Deal\Http\Controllers\V1
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2021
 */
class DealController extends Controller
{
    public DealServiceInterface $dealService;

    /**
     * @param DealServiceInterface $dealService DealServiceInterface
     */
    public function __construct(DealServiceInterface $dealService)
    {
        $this->dealService = $dealService;
    }

    /**
     * Name getUserDealDates
     *
     * @param GetSaleDatesRequest $request GetSaleDatesRequest
     * @param mixed               $id      User Id
     *
     * @return JsonResponse|object
     *
     * @throws AuthorizationException
     */
    public function getUserDealDates(GetSaleDatesRequest $request, $id)
    {
        /** @var User $sale */
        $sale = User::findOrFail($id);

        $this->authorize('getSaleDates', DealDate::class);

        $fields = $request->validated();

        return (DealDateResource::collection($this->dealService->getUserDealDates(
            $sale,
            Arr::get($fields, 'start', (Carbon::now())->toDateTimeString()),
            Arr::get($fields, 'end', (Carbon::now())->toDateTimeString())
        )))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Name getDealStatusTransitions
     *
     * @param mixed $id Deal Id
     *
     * @throws AuthorizationException
     */
    public function getDealStatusTransitions($id): JsonResponse
    {
        $deal = Deal::findOrFail($id);
        $this->authorize('getDealStatusTransitions', $deal);

        return response()
            ->json(DealStatusTransitionResource::collection(
                $deal
                    ->dealStatusTransitions()
                    ->orderBy('id', 'desc')
                    ->get()
            ))
            ->setStatusCode(Response::HTTP_OK);
    }
}

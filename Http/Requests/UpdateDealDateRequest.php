<?php

declare(strict_types=1);

namespace OctoCmsModule\Deal\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Description ...
 *
 * @link     https://www.allyoucancode.it
 *
 * @category Octo
 * @package  OctoCms\Deal\Http\Requests
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright All You Can Code 2021
 */
class UpdateDealDateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array|string[]
     */
    public function rules(): array
    {
        return [
            'start' => 'required|date_format:Y-m-d H:i:s',
            'end'   => 'required|date_format:Y-m-d H:i:s',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}

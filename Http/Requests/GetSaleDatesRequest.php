<?php

declare(strict_types=1);

namespace OctoCmsModule\Deal\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Description ...
 *
 * @link     https://octopus.srl
 *
 * @category Octo
 * @package  OctoCms\Core\Http\Requests
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 */
class GetSaleDatesRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array|mixed[]
     */
    public function rules(): array
    {
        return [
            'start' => 'date_format:Y-m-d',
            'end'   => 'date_format:Y-m-d',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}

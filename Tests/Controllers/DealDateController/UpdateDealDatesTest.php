<?php

namespace OctoCmsModule\Deal\Tests\Controllers\DealDateController;

use Carbon\Carbon;
use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Deal\Entities\DealDate;

/**
 * Class GetUserDealDatesTest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Deal\Tests\Controllers\DealController
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class UpdateDealDatesTest extends TestCase
{
    public function test_update()
    {
        $user = self::createAdminUser();

        Sanctum::actingAs($user);

        /** @var DealDate $dealDate */
        $dealDate = DealDate::factory([
            'date_start' => Carbon::now()->toDateTimeString(),
            'date_end'   => Carbon::now()->toDateTimeString(),
        ])->create();

        $start = Carbon::now()->addMinutes(30)->toDateTimeString();
        $end = Carbon::now()->addMinutes(40)->toDateTimeString();

        $response = $this->json(
            'PUT',
            route('deals.deal-dates.update', ['id' => $dealDate->id]),
            [
                'start' => $start,
                'end'   => $end
            ]
        );

        $response->assertStatus(Response::HTTP_OK);

        $this->assertDatabaseHas('deal_dates', [
            'id'         => $dealDate->id,
            'date_start' => $start,
            'date_end'   => $end
        ]);

    }


}

<?php

namespace OctoCmsModule\Deal\Tests\Controllers\DealController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Deal\Entities\Deal;

/**
 * Class GetUserDealDatesTest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Deal\Tests\Controllers\DealController
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class GetDealStatusTransitionsTest extends TestCase
{

    public function test_search_city_no_query()
    {
        $user = self::createAdminUser();
        /** @var Deal $deal */
        $deal = Deal::factory()->create();

        Sanctum::actingAs($user);
        $response = $this->json(
            'GET',
            route('deals.get-deal-status-transitions', ['id' => $deal->id]),
        );

        $response->assertStatus(Response::HTTP_OK);

    }


}

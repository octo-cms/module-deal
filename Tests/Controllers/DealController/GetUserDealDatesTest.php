<?php

namespace OctoCmsModule\Deal\Tests\Controllers\DealController;

use Carbon\Carbon;
use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GetUserDealDatesTest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Deal\Tests\Controllers\DealController
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class GetUserDealDatesTest extends TestCase
{
    public function test_search_city_no_query()
    {
        $user = self::createAdminUser();

        Sanctum::actingAs($user);
        $response = $this->json(
            'POST',
            route('deals.get-user-deal-dates', ['id' => $user->id]),
            [
                'start' => Carbon::now()->toDateString(),
                'end' => Carbon::now()->toDateString()
            ]
        );

        $response->assertStatus(Response::HTTP_OK);

    }


}

<?php

namespace OctoCmsModule\Deal\Tests\Entities;

use Illuminate\Database\Eloquent\Collection;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Deal\Entities\DealDate;
use OctoCmsModule\Deal\Entities\DealDetail;
use OctoCmsModule\Deal\Factories\DealDetailFactory;
use OctoCmsModule\Deal\Factories\DealFactory;
use Tests\TestCase;
use OctoCmsModule\Deal\Entities\Deal;


class DealDateTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testDealDateBelongsTo()
    {
        /** @var DealDate $dealDate */
        $dealDate=DealDate::factory()->create();
        $this->assertInstanceOf(Deal::class,$dealDate->deal);
    }

    public function testUserBelongsTo()
    {
        /** @var DealDate $dealDate */
        $dealDate=DealDate::factory()->create();
        $this->assertInstanceOf(User::class,$dealDate->user);
    }
}

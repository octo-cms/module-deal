<?php

namespace OctoCmsModule\Deal\Tests\Entities;

use Illuminate\Database\Eloquent\Collection;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Deal\Entities\DealDate;
use OctoCmsModule\Deal\Entities\DealDetail;
use OctoCmsModule\Lead\Entities\Lead;
use Tests\TestCase;
use OctoCmsModule\Deal\Entities\Deal;

/**
 * Class DealTest
 *
 * @category Octo
 * @package  OctoCmsModule\Deal\Tests\Entities
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class DealTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testDealBelongsTo()
    {
        /** @var Deal $deal */
        $deal = Deal::factory()
            ->has(DealDetail::factory()->count(3))
            ->has(DealDate::factory()->count(3))
            ->create();

        $this->assertCount(3,$deal->dealDetails);
        $this->assertInstanceOf(Collection::class, $deal->dealDetails);
        $this->assertInstanceOf(DealDetail::class, $deal->dealDetails()->first());

        $this->assertInstanceOf(Registry::class, $deal->registry);

        $user = $deal->user;
        $this->assertInstanceOf(User::class, $user);
        $this->assertInstanceOf(Collection::class, $user->deals()->get());
        $this->assertInstanceOf(Deal::class, $user->deals()->first());

        $lead = $deal->lead;
        $this->assertInstanceOf(Lead::class, $lead);
        $this->assertInstanceOf(Deal::class, $lead->deal()->first());

        $this->assertCount(3,$deal->dealDates);
        $this->assertInstanceOf(Collection::class, $deal->dealDates);
        $this->assertInstanceOf(DealDate::class, $deal->dealDates()->first());
    }
}

<?php

namespace OctoCmsModule\Deal\Tests\Entities;

use Illuminate\Database\Eloquent\Collection;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Deal\Entities\DealDetail;
use OctoCmsModule\Deal\Factories\DealDetailFactory;
use OctoCmsModule\Deal\Factories\DealFactory;
use Tests\TestCase;
use OctoCmsModule\Deal\Entities\Deal;


class DealDetailTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testDealDetailBelongsTo()
    {

        /** @var DealDetail $dealDetail */
        $dealDetail=DealDetail::factory()->create();

        $this->assertInstanceOf(Deal::class,$dealDetail->deal);

    }
}

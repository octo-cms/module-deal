<?php

namespace OctoCmsModule\Deal\Tests\Entities;

use Illuminate\Database\Eloquent\Collection;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Deal\Entities\Deal;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class RegistryTest
 *
 * @package OctoCmsModule\Deal\Tests\Entities
 */
class RegistryTest extends TestCase
{
    /**
     * Name testRegistryHasManyDeals
     *
     * @return void
     */
    public function testRegistryHasManyDeals()
    {
        /** @var Registry $registry */
        $registry = Registry::factory()
            ->has(Deal::factory()->count(3), 'deals')
            ->create();

        $registry->load('deals');

        $this->assertInstanceOf(Collection::class, $registry->deals);
        $this->assertInstanceOf(Deal::class, $registry->deals->first());
    }
}

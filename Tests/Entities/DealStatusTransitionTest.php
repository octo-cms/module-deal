<?php

namespace OctoCmsModule\Deal\Tests\Entities;

use Illuminate\Database\Eloquent\Collection;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Deal\Entities\DealDetail;
use OctoCmsModule\Deal\Entities\DealStatusTransition;
use OctoCmsModule\Deal\Factories\DealDetailFactory;
use OctoCmsModule\Deal\Factories\DealFactory;
use OctoCmsModule\Deal\Factories\DealStatusTransitionFactory;
use Tests\TestCase;
use OctoCmsModule\Deal\Entities\Deal;

/**
 * Class DealStatusTransitionTest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Deal\Tests\Entities
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class DealStatusTransitionTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testDealStatusTransitionBelongsTo()
    {

        /** @var DealStatusTransition $DealStatusTransition */
        $DealStatusTransition=DealStatusTransition::factory()->create();

        $this->assertInstanceOf(Deal::class,$DealStatusTransition->deal);
        $this->assertInstanceOf(User::class, $DealStatusTransition->user);


        $deal = $DealStatusTransition->deal()->first();
        $this->assertInstanceOf(Collection::class, $deal->DealStatusTransitions);
        $this->assertInstanceOf(DealStatusTransition::class, $deal->DealStatusTransitions->first());
    }
}

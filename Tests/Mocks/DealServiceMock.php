<?php

namespace OctoCmsModule\Deal\Tests\Mocks;

use Illuminate\Database\Eloquent\Collection;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Deal\Interfaces\DealServiceInterface;

/**
 * Class DealTest
 *
 * @category Octo
 * @package  OctoCmsModule\Deal\Tests\Entities
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class DealServiceMock implements DealServiceInterface
{
    /** @inheritDoc */
    public function getUserDealDates(User $user, string $start, string $end): Collection
    {
        return $user->dealDates()->get(2);
    }
}

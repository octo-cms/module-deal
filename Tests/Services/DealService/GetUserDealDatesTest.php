<?php

namespace OctoCmsModule\Deal\Tests\Services\DealService;

use Carbon\Carbon;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Deal\Entities\DealDate;
use OctoCmsModule\Deal\Services\DealService;
use Tests\TestCase;

/**
 * Class DealTest
 *
 * @category Octo
 * @package  OctoCmsModule\Deal\Tests\Entities
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class GetUserDealDatesTest extends TestCase
{

    /**
     * Name dataprovider
     *
     * @return array|mixed[]
     */
    public function dataprovider()
    {
        $providers = [];

        $providers[] = [
            (Carbon::now())->subHours(52)->toDateTimeString(),
            (Carbon::now())->subDays(50)->toDateTimeString(),
            false
        ];

//        $providers[] = [
//            (Carbon::now())->subHours(12)->toDateTimeString(),
//            (Carbon::now())->subHours(10)->toDateTimeString(),
//            true
//        ];
//
//        $providers[] = [
//            (Carbon::now())->addHours(10)->toDateTimeString(),
//            (Carbon::now())->addHours(12)->toDateTimeString(),
//            true
//        ];

        $providers[] = [
            (Carbon::now())->addHours(50)->toDateTimeString(),
            (Carbon::now())->addHours(52)->toDateTimeString(),
            false
        ];

        return $providers;
    }

    /**
     * A basic unit test example.
     *
     * @dataProvider dataprovider
     * @param mixed $dateStart Date Start
     * @param mixed $dateEnd   Date End
     * @param mixed $result    Result
     * @return void
     */
    public function testGetUserDealDatesTest($dateStart, $dateEnd, $result)
    {
        $service = new DealService();

        $start = (Carbon::now())->subDays(1)->toDateString();
        $end = (Carbon::now())->addDays(1)->toDateString();

        /** @var User $user */
        $user = User::factory()->create();

        DealDate::factory()->state([
            'user_id'    => $user->id,
            'date_start' => $dateStart,
            'date_end'   => $dateEnd,
        ])->create();

        if (!$result) {
            $this->assertEmpty($service->getUserDealDates($user, $start, $end));
        } else {
            $this->assertNotEmpty($service->getUserDealDates($user, $start, $end));
        }

    }
}

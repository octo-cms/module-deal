<?php

namespace OctoCmsModule\Deal\Traits;

use OctoCmsModule\Deal\Entities\DealDetail;

/**
 * Trait DetailableTrait
 *
 * @category Octo
 * @package  OctoCmsModule\Deal\Traits
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
trait DealDetailableTrait
{
    /**
     * Name details
     *
     * @return mixed
     */
    public function details()
    {
        return $this->morphOne(DealDetail::Class, 'detailable', 'detailable_type', 'detailable_id');
    }
}

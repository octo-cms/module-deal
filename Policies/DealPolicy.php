<?php

declare(strict_types=1);

namespace OctoCmsModule\Deal\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Interfaces\PermissionServiceInterface;

/**
 * @link     https://octopus.srl
 *
 * @category Octo
 * @package  OctoCmsModule\Lead\Policies
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 */
class DealPolicy
{
    use HandlesAuthorization;

    protected PermissionServiceInterface $permissionService;

    /**
     * @param PermissionServiceInterface $permissionService PermissionServiceInterface
     */
    public function __construct(PermissionServiceInterface $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    /**
     * Name getSaleDates
     *
     * @param User $user User Logged
     */
    public function getDealStatusTransitions(User $user): bool
    {
        return true;
    }
}

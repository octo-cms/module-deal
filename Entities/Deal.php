<?php

namespace OctoCmsModule\Deal\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Deal\Factories\DealFactory;
use OctoCmsModule\Lead\Entities\Lead;
use Plank\Mediable\Mediable;


/**
 * OctoCmsModule\Deal\Entities\Deal
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $lead_id
 * @property int|null $registry_id
 * @property string|null $status
 * @property string|null $sub_status
 * @property mixed|null $amount
 * @property string|null $notes
 * @property mixed|null $date_in
 * @property mixed|null $date_out
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OctoCmsModule\Deal\Entities\DealDate[] $dealDates
 * @property-read int|null $deal_dates_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\OctoCmsModule\Deal\Entities\DealDetail[] $dealDetails
 * @property-read int|null $deal_details_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\OctoCmsModule\Deal\Entities\DealStatusTransition[] $dealStatusTransitions
 * @property-read int|null $deal_status_transitions_count
 * @property-read Lead|null $lead
 * @property-read \Illuminate\Database\Eloquent\Collection|\OctoCmsModule\Core\Entities\Media[] $media
 * @property-read int|null $media_count
 * @property-read Registry|null $registry
 * @property-read User|null $user
 * @method static \Plank\Mediable\MediableCollection|static[] all($columns = ['*'])
 * @method static \OctoCmsModule\Deal\Factories\DealFactory factory(...$parameters)
 * @method static \Plank\Mediable\MediableCollection|static[] get($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|Deal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Deal newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Deal query()
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereDateIn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereDateOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereHasMedia($tags, bool $matchAll = false)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereHasMediaMatchAll(array $tags)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereLeadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereRegistryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereSubStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal withMedia($tags = [], bool $matchAll = false)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal withMediaMatchAll($tags = [])
 * @mixin \Eloquent
 */
class Deal extends Model
{
    use HasFactory;
    use Mediable;

    protected $table = "deal_deals";

    protected $fillable = [
        'status',
        'sub_status',
        'notes',
        'amount',
        'date_in',
        'date_out'
    ];

    protected $casts = [
        'amount'   => 'decimal:2',
        'date_in'  => 'date:Y-m-d',
        'date_out' => 'date:Y-m-d',
    ];

    protected static function booted()
    {
        static::creating(function (Deal $deal) {
            $deal->date_in = Carbon::now()->format('Y-m-d');
        });
    }

    /**
     * Name newFactory
     *
     * @return DealFactory
     */
    protected static function newFactory(): DealFactory
    {
        return DealFactory::new();
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Name dealDetails
     *
     * @return HasMany
     */
    public function dealDetails(): HasMany
    {
        return $this->hasMany(DealDetail::class);
    }

    /**
     * @return BelongsTo
     */
    public function registry(): BelongsTo
    {
        return $this->belongsTo(Registry::class);
    }

    /**
     * @return BelongsTo
     */
    public function lead(): BelongsTo
    {
        return $this->belongsTo(Lead::class);
    }

    /**
     * Name dealDates
     *
     * @return HasMany
     */
    public function dealDates(): HasMany
    {
        return $this->hasMany(DealDate::class)->orderByDesc('date_start');
    }

    /**
     * Name dealTransitions
     *
     * @return HasMany
     */
    public function dealStatusTransitions(): HasMany
    {
        return $this->hasMany(DealStatusTransition::class);
    }
}

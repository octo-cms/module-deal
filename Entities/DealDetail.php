<?php

namespace OctoCmsModule\Deal\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use OctoCms\Entities\SalaryDetail;
use OctoCms\Entities\Timesheet;
use OctoCmsModule\Deal\Factories\DealDetailFactory;

/**
 * Class DealDetail
 *
 * @category Octo
 * @package OctoCmsModule\Deal\Entities
 * @author Pasi Daniele <pasidaniele@gmail.com>
 * @license copyright Octopus Srl 2021
 * @link https://octopus.srl
 * @property int $id
 * @property int $deal_id
 * @property int|null $detailable_id
 * @property string|null $detailable_type
 * @property string|null $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \OctoCmsModule\Deal\Entities\Deal $deal
 * @property-read Model|\Eloquent $detailable
 * @method static \Illuminate\Database\Eloquent\Builder|DealDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DealDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DealDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|DealDetail whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealDetail whereDealId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealDetail whereDetailableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealDetail whereDetailableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealDetail whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \OctoCmsModule\Deal\Factories\DealDetailFactory factory(...$parameters)
 */
class DealDetail extends Model
{
    use HasFactory;

    protected $table="deal_deal_details";

    protected $fillable = [
        'deal_id',
        'detailable_id',
        'detailable_type',
        'amount'
    ];

    /**
     * Name newFactory
     *
     * @return DealDetailFactory
     */
    protected static function newFactory(): DealDetailFactory
    {
        return DealDetailFactory::new();
    }

    /**
     * Name booted
     *
     * @return void
     */
    protected static function booted()
    {
        static::saved(function (DealDetail $dealDetail) {
            $dealDetail->deal()->update([
                'amount' => DealDetail::where('deal_id', '=', $dealDetail->deal_id)
                    ->sum('amount')
            ]);
        });

        static::deleted(function (DealDetail $dealDetail) {
            $dealDetail->deal()->update([
                'amount' => DealDetail::where('deal_id', '=', $dealDetail->deal_id)
                    ->sum('amount')
            ]);
        });
    }

    /**
     * Name deal
     *
     * @return BelongsTo
     */
    public function deal(): BelongsTo
    {
        return $this->belongsTo(Deal::class);
    }

    /**
     * Name detailable
     *
     * @return MorphTo
     */
    public function detailable(): MorphTo
    {
        return $this->morphTo(__FUNCTION__, 'detailable_type', 'detailable_id', 'id');
    }
}

<?php

namespace OctoCmsModule\Deal\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Deal\Factories\DealStatusTransitionFactory;


/**
 * OctoCmsModule\Deal\Entities\DealStatusTransition
 *
 * @property int $id
 * @property int $deal_id
 * @property int|null $user_id
 * @property string|null $status_from
 * @property string|null $sub_status_from
 * @property string|null $status_to
 * @property string|null $sub_status_to
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \OctoCmsModule\Deal\Entities\Deal $deal
 * @property-read User|null $user
 * @method static \OctoCmsModule\Deal\Factories\DealStatusTransitionFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|DealStatusTransition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DealStatusTransition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DealStatusTransition query()
 * @method static \Illuminate\Database\Eloquent\Builder|DealStatusTransition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealStatusTransition whereDealId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealStatusTransition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealStatusTransition whereStatusFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealStatusTransition whereStatusTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealStatusTransition whereSubStatusFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealStatusTransition whereSubStatusTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealStatusTransition whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealStatusTransition whereUserId($value)
 * @mixin \Eloquent
 */
class DealStatusTransition extends Model
{
    use HasFactory;

    protected $table = "deal_deal_status_transitions";

    protected $fillable = [
        'status_from',
        'sub_status_from',
        'status_to',
        'sub_status_to',
    ];

    /**
     * Name newFactory
     *
     * @return DealStatusTransitionFactory
     */
    protected static function newFactory()
    {
        return DealStatusTransitionFactory::new();
    }

    /**
     * Name deal
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deal()
    {
        return $this->belongsTo(Deal::class);
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

<?php

declare(strict_types=1);

namespace OctoCmsModule\Deal\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Deal\Factories\DealDateFactory;


/**
 * OctoCmsModule\Deal\Entities\DealDate
 *
 * @property-read \OctoCmsModule\Deal\Entities\Deal $deal
 * @property-read User                              $user
 * @method static \Illuminate\Database\Eloquent\Builder|DealDate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DealDate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DealDate query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $deal_id
 * @property int|null $user_id
 * @property \datetime $date_start
 * @property \datetime $date_end
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|DealDate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealDate whereDateEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealDate whereDateStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealDate whereDealId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealDate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealDate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealDate whereUserId($value)
 * @method static \OctoCmsModule\Deal\Factories\DealDateFactory factory(...$parameters)
 * @property string|null $status
 * @method static \Illuminate\Database\Eloquent\Builder|DealDate whereStatus($value)
 * @property string|null $text
 * @method static \Illuminate\Database\Eloquent\Builder|DealDate whereText($value)
 */
class DealDate extends Model
{
    use HasFactory;

    protected $table = 'deal_dates';

    protected $fillable = [
        'date_start',
        'date_end',
        'status',
        'text'
    ];

    protected $casts = [
        'date_start' => 'datetime:Y-m-d H:i:s',
        'date_end'   => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * Name newFactory
     *
     * @return mixed
     */
    protected static function newFactory()
    {
        return DealDateFactory::new();
    }

    /**
     * Name deal
     */
    public function deal(): BelongsTo
    {
        return $this->belongsTo(Deal::class);
    }

    /**
     * Name user
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}

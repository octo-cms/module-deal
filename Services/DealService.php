<?php

declare(strict_types=1);

namespace OctoCmsModule\Deal\Services;

use Illuminate\Database\Eloquent\Collection;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Deal\Interfaces\DealServiceInterface;

/**
 * @link     https://www.allyoucancode.it
 *
 * @category Octo
 * @package  OctoCms\Core\Services
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright All You Can Code 2020
 */
class DealService implements DealServiceInterface
{
    /** @inheritDoc */
    public function getUserDealDates(User $user, string $start, string $end): Collection
    {
        return $user->dealDates()
            ->where('date_start', '>=', $start)
            ->where('date_end', '<=', $end)
            ->get();
    }
}

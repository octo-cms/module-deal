<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'admin/v1'], function () {

    Route::group(['middleware' => ['auth:sanctum']], function () {

        Route::group(['prefix' => 'deals'], function () {

            Route::group(['prefix' => '{id}'], function () {

                Route::get('deal-status-transitions',
                    getRouteAction("Deal", "V1\DealController", 'getDealStatusTransitions')
                )->name('deals.get-deal-status-transitions');

                Route::post('get-user-deal-dates',
                    getRouteAction("Deal", "V1\DealController", 'getUserDealDates')
                )->name('deals.get-user-deal-dates');
            });

        });

        Route::group(['prefix' => 'deal-dates'], function () {
            Route::put('{id}',
                getRouteAction("Deal", "V1\DealDateController", 'update')
            )->name('deals.deal-dates.update');
        });

    });
});

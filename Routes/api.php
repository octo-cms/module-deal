<?php

use Illuminate\Http\Request;
use OctoCmsModule\Deal\Http\Controllers\V1\DealController;
use OctoCmsModule\Deal\Http\Controllers\V1\DealDateController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'admin/v1'], function () {

    Route::group(['middleware' => ['auth:sanctum']], function () {

        Route::group(['prefix' => 'deals'], function () {

            Route::group(['prefix' => '{id}'], function () {

                Route::get('deal-status-transitions',[DealController::class, 'getDealStatusTransitions'])
                    ->name('deals.get-deal-status-transitions');

                Route::post('get-user-deal-dates', [DealController::class, 'getUserDealDates'])
                    ->name('deals.get-user-deal-dates');
            });

        });

        Route::group(['prefix' => 'deal-dates'], function () {
            Route::put('{id}', [DealDateController::class, 'update'])
                ->name('deals.deal-dates.update');
        });

    });
});

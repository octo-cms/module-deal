<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @link     https://octopus.srl
 *
 * @category Octo
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 */
class CreateDealDatesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create(
            'deal_dates',
            static function (Blueprint $table): void {
                $table->bigIncrements('id');
                $table->bigInteger('deal_id')->unsigned()->nullable();
                $table->bigInteger('user_id')->unsigned()->nullable();
                $table->dateTime('date_start');
                $table->dateTime('date_end');
                $table->string('status', 20)->nullable();
                $table->string('text')->nullable();

                $table->timestamps();

                $table->foreign('user_id')->references('id')
                    ->on('users')->onDelete('cascade');

                $table->foreign('deal_id')->references('id')
                    ->on('deal_deals')->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sale_dates');
    }
}

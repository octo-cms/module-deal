<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deal_deal_details', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('deal_id')->unsigned();
            $table->bigInteger('detailable_id')->unsigned()->nullable();
            $table->string('detailable_type')->nullable();
            $table->decimal('amount',8,2)->nullable();
            $table->timestamps();

            $table->foreign('deal_id')->references('id')
                ->on('deal_deals')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deal_deal_details');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deal_deals', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->bigInteger('lead_id')->unsigned()->nullable();
            $table->bigInteger('registry_id')->unsigned()->nullable();
            $table->string('status', 40)->nullable();
            $table->string('sub_status', 40)->nullable();
            $table->decimal('amount',8,2)->nullable();
            $table->text('notes')->nullable();
            $table->date('date_in')->nullable();
            $table->date('date_out')->nullable();

            $table->timestamps();

            $table->foreign('user_id')->references('id')
                ->on('users')->onDelete('set null');
            $table->foreign('lead_id')->references('id')
                ->on('lead_leads')->onDelete('set null');
            $table->foreign('registry_id')->references('id')
                ->on('registry')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deal_deals');
    }
}

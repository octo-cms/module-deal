<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealStatusTransitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deal_deal_status_transitions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('deal_id')->unsigned();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->string('status_from', 40)->nullable();
            $table->string('sub_status_from', 40)->nullable();
            $table->string('status_to', 40)->nullable();
            $table->string('sub_status_to', 40)->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')
                ->on('users')->onDelete('set null');

            $table->foreign('deal_id')->references('id')
                ->on('deal_deals')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deal_deal_transitions');
    }
}

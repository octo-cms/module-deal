<?php

declare(strict_types=1);

namespace OctoCmsModule\Deal\Interfaces;

use Illuminate\Database\Eloquent\Collection;
use OctoCmsModule\Core\Entities\User;

/**
 * @link     https://octopus.srl
 *
 * @category Octo
 * @package  OctoCms\Core\Interfaces
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 */
interface DealServiceInterface
{
    /**
     * Name getUserDealDates
     *
     * @param User   $user  Sale
     * @param string $start Date Start in format Y-m-d
     * @param string $end   Date End in format Y-m-d
     */
    public function getUserDealDates(User $user, string $start, string $end): Collection;
}

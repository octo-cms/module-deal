<?php

declare(strict_types=1);

namespace OctoCmsModule\Deal\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Deal\Entities\Deal;
use OctoCmsModule\Deal\Entities\DealStatusTransition;

/**
 * Description ...
 *
 * @link     https://octopus.srl
 *
 * @category Octo
 * @package  OctoCmsModule\Deal\Factories
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2021
 */
class DealStatusTransitionFactory extends Factory
{
    //phpcs:disable
    /**
     * The name of the factory's corresponding model.
     */
    protected $model = DealStatusTransition::class;
    //phpcs:enable

    /**
     * Define the model's default state.
     *
     * @return array|mixed[]
     */
    public function definition(): array
    {
        return [
            'deal_id'         => Deal::factory(),
            'status_from'     => 'from',
            'sub_status_from' => 'sub_from',
            'status_to'       => 'to',
            'sub_status_to'   => 'sub_to',
            'user_id'         => User::factory(),
        ];
    }
}

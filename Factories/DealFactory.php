<?php

namespace OctoCmsModule\Deal\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\Provider;
use OctoCmsModule\Deal\Entities\Deal;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Lead\Entities\Lead;

/**
 * Class DealFactory
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Deal\Factories
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class DealFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Deal::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'lead_id'     => Lead::factory(),
            'registry_id' => Registry::factory(),
            'user_id'     => User::factory(),
            'status'      => '',
            'sub_status'  => '',
            'notes'       => '',
            'amount'      => $this->faker->numberBetween(1, 400)
        ];
    }
}


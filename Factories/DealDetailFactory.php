<?php

namespace OctoCmsModule\Deal\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Deal\Entities\Deal;
use OctoCmsModule\Deal\Entities\DealDetail;

/**
 * Class DealDetailFactory
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Deal\Factories
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class DealDetailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DealDetail::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'deal_id'     => Deal::factory(),
            'amount'      => $this->faker->numberBetween(1,400)
        ];
    }
}


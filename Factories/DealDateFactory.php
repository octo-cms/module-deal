<?php

declare(strict_types=1);

namespace OctoCmsModule\Deal\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Deal\Entities\Deal;
use OctoCmsModule\Deal\Entities\DealDate;

/**
 * @link     https://octopus.srl
 *
 * @category Octo
 * @package  OctoCmsModule\Deal\Factories
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 */
class DealDateFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     */
    protected $model = DealDate::class;

    /**
     * Define the model's default state.
     *
     * @return array|mixed[]
     */
    public function definition(): array
    {
        return [
            'deal_id'    => Deal::factory(),
            'user_id'    => User::factory(),
            'date_start' => $this->faker->date('Y-m-d H:i:s'),
            'date_end'   => $this->faker->date('Y-m-d H:i:s'),
            'status'     => null,
            'text'     => null
        ];
    }
}

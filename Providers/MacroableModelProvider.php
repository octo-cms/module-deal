<?php

declare(strict_types=1);

namespace OctoCmsModule\Deal\Providers;

use Illuminate\Support\ServiceProvider;
use Javoscript\MacroableModels\Facades\MacroableModels;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Deal\Entities\Deal;
use OctoCmsModule\Deal\Entities\DealDate;
use OctoCmsModule\Lead\Entities\Lead;

/**
 * Description ...
 *
 * @link     https://octopus.srl
 *
 * @category Octo
 * @package  OctoCmsModule\Deal\Providers
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 */
class MacroableModelProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        /**
         * USER RELATIONS
         **/
        MacroableModels::addMacro(User::class, 'deals', function () {
            return $this->hasMany(Deal::class, 'user_id', 'id');
        });

        MacroableModels::addMacro(User::class, 'dealDates', function () {
            return $this->hasMany(DealDate::class, 'user_id', 'id');
        });

        /**
         * LEAD RELATIONS
         **/
        MacroableModels::addMacro(Lead::class, 'deal', function () {
            return $this->hasOne(Deal::class, 'lead_id', 'id');
        });

        /**
         * REGISTRY RELATIONS
         **/
        MacroableModels::addMacro(Registry::class, 'deals', function () {
            return $this->hasMany(Deal::class);
        });
    }
}

<?php

declare(strict_types=1);

namespace OctoCmsModule\Deal\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OctoCmsModule\Core\Transformers\UserResource;

/**
 * Description ...
 *
 * @link     https://octopus.srl
 *
 * @category Octo
 * @package  OctoCmsModule\Deal\Transformers
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2021
 */
class DealStatusTransitionResource extends JsonResource
{
    //phpcs:disable
    /**
     * Name toArray
     *
     * @param Request $request Request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'          => $this->id,
            'deal_id'     => $this->deal_id,
            'user_id'     => $this->user_id,
            'status_from' => $this->status_from,
            'status_to'   => $this->status_to,
            'date'        => optional($this->created_at)->format('Y-m-d'),
            'time'        => optional($this->created_at)->format('H:i:s'),
            'deal'        => new DealResource($this->whenLoaded('deal')),
            'user'        => new UserResource($this->whenLoaded('user'))
        ];
    }
    //phpcs:enable
}

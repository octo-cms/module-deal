<?php

declare(strict_types=1);

namespace OctoCmsModule\Deal\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;
use OctoCmsModule\Core\Transformers\UserResource;

use function optional;

/**
 * Description ...
 *
 * @link     https://octopus.srl
 *
 * @category Octo
 * @package  OctoCmsModule\Deal\Transformers
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2021
 */
class DealDateResource extends JsonResource
{
    /**
     * Name toArray
     *
     * @return array|mixed[]
     */
    public function toArray($request): array
    {
        // phpcs:disable
        return [
            'id'         => $this->id,
            'deal_id'    => $this->deal_id,
            'user_id'    => $this->user_id,
            'date_start' => optional($this->date_start)->toDateTimeString(),
            'date_end'   => optional($this->date_end)->toDateTimeString(),
            'status'     => $this->status,
            'text'       => $this->text,
            'user'       => new UserResource($this->whenLoaded('user')),
            'deal'       => new DealResource($this->whenLoaded('deal')),
        ];
        // phpcs:enable
    }
}

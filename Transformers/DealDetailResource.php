<?php

namespace OctoCmsModule\Deal\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

use OctoCmsModule\Core\Transformers\RegistryResource;
use OctoCmsModule\Core\Transformers\UserResource;
use OctoCmsModule\Lead\Transformers\LeadResource;

/**
 * Class DealDetailResource
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Deal\Transformers
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class DealDetailResource extends JsonResource
{
    /**
     * Name toArray
     *
     * @param Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'deal_id'     => $this->deal_id,
            'amount'      => $this->amount,
            'deal'        => new DealResource($this->whenLoaded('deal'))
        ];
    }
}

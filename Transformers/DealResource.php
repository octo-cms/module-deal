<?php

declare(strict_types=1);

namespace OctoCmsModule\Deal\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;
use OctoCmsModule\Core\Transformers\RegistryResource;
use OctoCmsModule\Core\Transformers\UserResource;
use OctoCmsModule\Lead\Transformers\LeadResource;

/**
 * Description ...
 *
 * @link     https://octopus.srl
 *
 * @category Octo
 * @package  OctoCmsModule\Deal\Transformers
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2021
 */
class DealResource extends JsonResource
{
    //phpcs:disable
    /**
     * Name toArray
     *
     * @return array|mixed[]
     */
    public function toArray($request): array
    {

        return [
            'id'               => $this->id,
            'registry_id'      => $this->registry_id,
            'user_id'          => $this->user_id,
            'lead_id'          => $this->lead_id,
            'amount'           => $this->amount,
            'status'           => $this->status,
            'date_in'          => $this->date_in,
            'date_out'         => $this->date_out,
            'registry'         => new RegistryResource($this->whenLoaded('registry')),
            'user'             => new UserResource($this->whenLoaded('user')),
            'lead'             => new LeadResource($this->whenLoaded('lead')),
            'deal_details'     => new DealDetailResource($this->whenLoaded('dealDetails')),
            'deal_transitions' => new DealStatusTransitionResource($this->whenLoaded('dealTransitions'))
        ];

    }
    //phpcs:enable
}
